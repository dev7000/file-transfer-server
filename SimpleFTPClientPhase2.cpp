#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <iostream>
#include <unistd.h>
using namespace std;



int main(int p, char* c[]){
	if(p!=3){
		fprintf(stderr, "%s <serverIPAddr:port> <fileToReceive>\n",c[0]);
		exit(1);
	}
	struct sockaddr_in serv_addr;
    int sock = socket(AF_INET, SOCK_STREAM, 0);
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;

	int i;
	string ip;
	i=0;
	while(c[1][i]!=':'){
		ip[i]=c[1][i];
		i++;
	}  
	const char* IP=ip.c_str();
	char* port= c[1]+i+1;
    serv_addr.sin_port = htons(atoi(port));
      
    // Convert IPv4 and IPv6 addresses from text to binary form
    inet_pton(AF_INET, IP, &serv_addr.sin_addr);
  
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
    	//printf("Client: %s:%d\n",inet_ntoa(serv_addr.sin_addr),ntohs(serv_addr.sin_port) )  
        perror("Connection Failed");
        exit(2);
    }else{
    	printf("ConnectDone: %s\n",c[1]);
    }

    char buffer[4096];
    int valread;
    string s1;
    string s2(c[2]);
    s1="get "+s2;

    int z;
    z=send(sock,s1.c_str(),s1.length(),0);

    FILE* file;
    i=0;
	while(c[2][i])i++;
	string fl;
	fl[0]='.';fl[1]='/';
	for(int j=0;j<i;j++){
		fl[j+2]=c[2][j];
	}
	file = fopen(fl.c_str(),"w+");
	if(file == NULL){
		perror("File Error");
		exit(3);
	}

    i=0;
    do{
    	valread = recv( sock , buffer, 4096,0);
    	
      	if(valread!=-1){
    		i=i+valread;
    		for(int j=0;j<valread;j++){
    		//printf("%c",buffer[j]);	
    		fprintf(file,"%c",buffer[j] );	
    	    }
            //sleep(10);
        }
    
    }while(valread!=0);
    
    printf("FileWritten: %d bytes\n",i );
    fclose(file);

		
}