#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <iostream>
#include <unistd.h>
using namespace std;



int main(int p, char* c[]){
	if(p!=5){
		fprintf(stderr, "%s <serverIPAddr:port> <op> <fileToReceive> <receiveInternal>\n",c[0]);
		exit(1);
	}
	struct sockaddr_in serv_addr;
    int sock = socket(AF_INET, SOCK_STREAM, 0);
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;

	int i;
	string ip;
	i=0;
	while(c[1][i]!=':'){
		ip[i]=c[1][i];
		i++;
	}  
	const char* IP=ip.c_str();
	char* port= c[1]+i+1;
    serv_addr.sin_port = htons(atoi(port));
      
    // Convert IPv4 and IPv6 addresses from text to binary form
    inet_pton(AF_INET, IP, &serv_addr.sin_addr);
  
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
    	//printf("Client: %s:%d\n",inet_ntoa(serv_addr.sin_addr),ntohs(serv_addr.sin_port) )  
        perror("Connection Failed");
        exit(2);
    }else{
    	printf("ConnectDone: %s\n",c[1]);
    }

    string op(c[2]);

    if(op=="get"){
        char buffer[4096];
        int valread;
        string s1;
        string s2(c[3]);
        s1="get "+s2+'\0';

        int z;
        z=send(sock,s1.c_str(),s1.length()+1,0);

        FILE* file;
        
    	
    	file = fopen(c[3],"w+");
    	if(file == NULL){
    		perror("File Error");
    		exit(3);
    	}

        i=0;
        do{
        	valread = recv( sock , buffer, 4096,0);
        	
          	if(valread!=-1){
        		i=i+valread;
        		for(int j=0;j<valread;j++){
        		//printf("%c",buffer[j]);	
        		fprintf(file,"%c",buffer[j] );	
        	    }
                usleep(atoi(c[4])*1000);
            }
        
        }while(valread!=0);
        
        printf("FileWritten: %d bytes\n",i );
        fclose(file);
    }else if(op == "put"){
        char buffer[4096];
        int y,d,z,q,size=0;

        string s1;
        string s2(c[3]);
        s1="put "+s2;

        z=send(sock,s1.c_str(),s1.length()+1,0);
       // z=send(sock,cd,8,0);
        FILE* file;
        file = fopen(c[3],"rb");
                if(file == NULL){
                    printf("FileTransferFail\n");
                    perror("File Error");
                    shutdown(sock,2);
                }else
        y=0;z=0;d=0;q=0;
        do{
            d=fgetc(file);
            buffer[size] = d;   
            size++;
            if(size==4096){
                z=0;
                do{
                    q=4096-z;
                    z+=send(sock,buffer+z,q,0);
                }while(z!=4096);
                size=0;
                y+=4096;
            }
        }while (d!= EOF);

        z=0;
        do{
            q=size-1-z;
            z+=send(sock,buffer+z,size-1-z,0);
        }while(z!=size-1);
        y+=size-1;

        printf("TransferDone: %d bytes\n",y);
        fclose(file);
    }
		
}