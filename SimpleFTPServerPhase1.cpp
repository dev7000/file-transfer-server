#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace std;



int main(int p, char* c[]){
	if(p!=3){
		fprintf(stderr, "%s <portNum> <fileToTransfer>\n",c[0]);
		exit(1);
	}
	FILE* file;
	int i=0;
	while(c[2][i])i++;
	char fl[i+3];
	fl[0]='.';fl[1]='/';
	for(int j=0;j<i;j++){
		fl[j+2]=c[2][j];
	}
	file = fopen(fl,"r");
	if(file == NULL){
		perror("File Error");
		exit(3);
	}
	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
	struct sockaddr_in my_addr;    // my address information
	struct sockaddr_in their_addr; // connector's address information
	int sin_size;
	sockfd = socket(PF_INET, SOCK_STREAM, 0); 
	my_addr.sin_family = AF_INET;         // host byte order
	my_addr.sin_port = htons(atoi(c[1]));     // short, network byte order
	my_addr.sin_addr.s_addr = INADDR_ANY;
	//inet_aton("127.0.0.5",&my_addr.sin_addr); // autofill with my IP
	memset(&(my_addr.sin_zero), '\0', 8); // zero the rest of the struct
	
	if(bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))==-1){
		perror("Bind Error");
		exit(2);
	}else {cout<<"BindDone: ";cout<<c[1]<<endl;}

	if(listen(sockfd,10)>-1){cout<<"ListenDone: ";cout<<c[1]<<endl; }
	sin_size = sizeof(struct sockaddr_in);
	new_fd = accept(sockfd, (struct sockaddr *)&their_addr, (socklen_t*)&sin_size);
	char ch[4096];
	char d;
	int size=0;
	printf("Client: %s:%d\n",inet_ntoa(their_addr.sin_addr),ntohs(their_addr.sin_port) );
	int y=0,z,q;
    do{
     	d=fgetc(file);
     	ch[size] = d;   
      	size++;
      	if(size==4096){
      		z=0;
      		do{
      			q=4096-z;
      			z=send(new_fd,ch+z,q,0);
      			y+=z;
      		}while(z!=q);
      		size=0;
      	}
    }while (d!= EOF);

    z=0;
	do{
		q=size-1-z;
		z=send(new_fd,ch+z,size-1-z,0);
		y+=z;
	}while(z!=q);

	printf("TransferDone: %d bytes\n",y);
    fclose(file);

		
}