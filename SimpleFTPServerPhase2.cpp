#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
using namespace std;



int main(int p, char* c[]){
	if(p!=2){
		fprintf(stderr, "%s <portNum> \n",c[0]);
		exit(1);
	}
	FILE* file;

	int sockfd, new_fd;  // listen on sock_fd, new connection on new_fd
	struct sockaddr_in my_addr;    // my address information
	struct sockaddr_in their_addr; // connector's address information
	int sin_size;
	sockfd = socket(PF_INET, SOCK_STREAM, 0); 
	my_addr.sin_family = AF_INET;         // host byte order
	my_addr.sin_port = htons(atoi(c[1]));     // short, network byte order
	my_addr.sin_addr.s_addr = INADDR_ANY;
	//inet_aton("127.0.0.5",&my_addr.sin_addr); // autofill with my IP
	memset(&(my_addr.sin_zero), '\0', 8); // zero the rest of the struct
	
	if(bind(sockfd, (struct sockaddr *)&my_addr, sizeof(struct sockaddr))==-1){
		perror("Bind Error");
		exit(2);
	}else {cout<<"BindDone: ";cout<<c[1]<<endl;}
		while(true){

		if(listen(sockfd,10)>-1){cout<<"ListenDone: ";cout<<c[1]<<endl; }
		sin_size = sizeof(struct sockaddr_in);
		new_fd = accept(sockfd, (struct sockaddr *)&their_addr, (socklen_t*)&sin_size);
		char ch1[4096],ch2[4096];
		int d;
		int size=0;
		printf("Client: %s:%d\n",inet_ntoa(their_addr.sin_addr),ntohs(their_addr.sin_port) );
		int y=0,z,q,valread;

		valread = recv( new_fd , ch1, 4096,0);

		string s;
		int i=0;
		while(i<4){
			s=s+ch1[i];
			i++;
		}
	/**	if(s!="get "){
			printf("%d\n",(int)s.length() );
			printf("%s\n", s.c_str());
			printf("UnknownCmd\n");
			shutdown(new_fd,2);
			continue;
		}**/
		if(s!="get "){
				printf("%d\n",(int)s.length() );
				printf("%s\n", s.c_str());
				printf("UnknownCmd\n");
				fprintf(stderr, "UnknownCmd");
				shutdown(new_fd,2);
				continue;
			}	
		while(i<valread){
			s[i-4]=ch1[i];
			i++;
		}
		s[i-4]='\0';

		printf("FileRequested: %s\n",s.c_str());
		file = fopen(s.c_str(),"rb");
			
		if(file == NULL){
			printf("FileTransferFail\n");
			perror("File Error");
			shutdown(new_fd,2);
			continue;
		}	

	    do{
	     	d=fgetc(file);
	     	ch2[size] = d;   
	      	size++;
	      	if(size==4096){
	      		z=0;
	      		do{
	      			q=4096-z;
	      			z+=send(new_fd,ch2+z,q,0);
	      		}while(z!=4096);
	      		size=0;
	      		y+=4096;
	      	}
	    }while (d!= EOF);

	    z=0;
		do{
			q=size-1-z;
			z+=send(new_fd,ch2+z,size-1-z,0);
		}while(z!=size-1);
		y+=size-1;

		printf("TransferDone: %d bytes\n",y);
		fclose(file);
		shutdown(new_fd,2);
	}
   

		
}